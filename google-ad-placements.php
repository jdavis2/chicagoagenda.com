<script type='text/javascript'>
googletag.cmd.push(function() {
googletag.defineOutOfPageSlot('/61924087/chicagoagenda.com', 'div-gpt-ad-interstitial')
.addService(googletag.pubads()).setTargeting("pos","1x1");
googletag.defineSlot('/61924087/chicagoagenda.com', [728, 90], 'div-gpt-ad-top1')
.addService(googletag.pubads()).setTargeting("pos","atf leaderboard");
googletag.defineSlot('/61924087/chicagoagenda.com', [728, 90], 'div-gpt-ad-btf')
.addService(googletag.pubads()).setTargeting("pos","btf leaderboard");
googletag.pubads().enableSingleRequest();
googletag.enableServices();
});
</script>