<link rel="stylesheet" type="text/css" href="http://www.suntimes.com/csp/cms/sites/STM/assets/css/global.css?v20130625" media="all"/>

			
	</div>
				 
				 
				 <div id="footer-links-wrapper" class="tk-proxima-nova">
					<div id="footer-links-content">
						<div class="divided site">
							
							
							<h4 class="tk-proxima-nova"><a href="http://www.suntimes.com" style="color:#333;text-decoration:none;">CHICAGO SUN-TIMES</a></h4> <!-- Names may change. -->
								
							<ul>
								
										
							<li><a href='http://www.suntimes.com/aboutus/contactus/index.html'>Send Feedback</a></li>
							<li><a href='http://www.suntimes.com/aboutus/contactus/index.html'>Contact Us</a></li>
							<li><a href='http://www.suntimes.com/aboutus/index.html'>About Us</a></li>
							<li><a href='http://www.suntimes.com/advertiser'>Advertise with Us</a></li>
							<li><a href='http://www.suntimes.com/stayconnected/index.html#alerts'>Text Alerts</a></li>	
							<li><a href='http://www.suntimes.com/register'>Newsletters</a></li>	
															
							</ul>
						</div>
					</div>

			<div id="footer-links-content">
						<div class="divided paper">
							<h4>&nbsp;</h4>
								<ul>
										
								
								<li><a href="https://register.suntimesnewsgroup.com/clickshare/checkDelivery.do">Subscribe</a></li>										
								<li><a href="http://www.suntimes.com/aboutus/service/index.html">Reader Services</a></li>									
								<li><a href="http://www.suntimes.com/epaper">Today's Paper</a></li>
								<li><a href="http://www.suntimesreprints.com">Licensing</a></li>										
								<li><a href="http://www.suntimesreprints.com">Reprints</a></li>	
																
							</ul>
						</div>
						</div>

				<div id="footer-links-content">
						<div class="divided affiliates">
							<h4>Company</h4>
							<ul>
							<li><a href="http://www.suntimes.com/aboutus/terms/index.html" title="Terms of Use">Terms</a></li>
							<li><a href="http://www.suntimes.com/aboutus/privacypolicy/index.html" title="Privacy Policy">Privacy</a></li>
							<li><a href="http://www.suntimes.com/aboutus/submissionguide/index.html" title="Submission Guidelines">Submissions</a></li>
							<li><a href="http://www.suntimes.com/consumerservices/faq/index.html" title="Site Index">FAQ</a></li>
						</ul>
						</div>
				   </div>
				<div id="footer-links-content">
						<div class="divided express-links">
							<h4>Express Links</h4>
							<ul>
									
							<li><a href='http://voices.suntimes.com/'>Voices - Blogs</a></li>
							<li><a href='http://www.suntimes.com/video/index.html'>Video</a></li>
							<li><a href='http://legacy.suntimes.com/obituaries/chicagosuntimes/'>Obituaries</a></li>
							<li><a href='http://www.suntimes.com/lifestyles/crossword/index.html'>Crosswords</a></li>
									
							</ul>				
						</div>
					</div>
						<div id="footer-links-content">
						<div class="divided express-links">
							<h4>&nbsp;</h4>
								
							<ul>
							<li><a href='http://www.suntimes.com/lifestyles/horoscopes/index.html'>Horoscopes</a></li>
							<li><a href='http://www.suntimes.com/entertainment/sudoku/index.html'>Sudoku</a></li>
							<li><a href='http://www.suntimes.com/marketplace'>Classifieds</a></li>
							<li><a href='http://chicagosuntimes.mycapture.com/mycapture/index.asp'>Buy Photos</a></li>
							</ul>	
										
						</div>
					</div>
					<style>
						#footer-links-content img {
								display:inline !important:
						}
					</style>
					
					 <div id="footer-links-content">
						<div class="padded-footer-connect">
							<h4>Connect with us</h4><a href="http://www.suntimes.com/stayconnected/"><img src="http://www.suntimes.com/csp/cms/sites/STM/assets/img/footerfacebookBlue.png" width="17" height="17"/></a><a href="http://www.suntimes.com/stayconnected/"><img src="http://www.suntimes.com/csp/cms/sites/STM/assets/img/footertwitterBlue.png" width="17" height="17" style="display:inline;"/></a>			
						</div>
						</div>
						
					</div>
					
					
					<div class="pubs-sub-footer-gray">
						<div class="pointercenter" id="click">
						<img src="http://www.suntimes.com/csp/cms/sites/STM/assets/img/arrowsDown.gif"></div>
			<div class="footerHidden">
			<div id="pubs-sub-footer-wrapper">
			
<div class="pubtextcenter">
<img src="http://www.suntimes.com/csp/cms/sites/STM/assets/img/footer_ourpubs960_ccc.png" width="960" height="26"/></div>
					<div id="pubs-sub-footer-content">
						<div class="divided-daily-pubs">
							<h4>DAILY PUBLICATIONS</h4>
							<ul>
							<li><a href="http://www.suntimes.com/index.html">Chicago Sun-Times</a></li>
							<li><a href="http://beaconnews.suntimes.com/index.html">The Beacon News</a></li>
							<li><a href="http://couriernews.suntimes.com/index.html">The Courier News</a></li>
							<li><a href="http://heraldnews.suntimes.com/index.html">The Herald News</a></li>
							<li><a href="http://newssun.suntimes.com/index.html">Lake County News-Sun</a></li>
							<li><a href="http://napervillesun.suntimes.com/index.html">The Naperville Sun</a></li>
							<li><a href="http://www.post-trib.com/index.html">Post-Tribune</a></li>
							<li><a href="http://www.southtownstar.com/index.html">The SouthtownStar</a></li>
								</ul>
						</div>

			   
						<div class="divided-pioneer-pubs">
							<h4>PIONEER PRESS - PIONEER LOCAL</h4>
							<ul>
							<li><a href="http://www.pioneerlocal.com/index.html">PioneerLocal.com</a></li>                	
							<li><a href="http://www.pioneerlocal.com/barrington/index.html">Barrington Courier Review</a></li>
							<li><a href="http://www.pioneerlocal.com/buffalogrove/index.html">Buffalo Grove Countryside</a></li>
							<li><a href="http://www.pioneerlocal.com/clarendonhills/index.html">The Doings Claredon Hills Edition</a></li> 	
							<li><a href="http://www.pioneerlocal.com/hinsdale/index.html">The Doings Hinsdale Edition</a></li>
							<li><a href="http://www.pioneerlocal.com/lagrange/index.html">The Doings La Grange Edition</a></li>
							<li><a href="http://www.pioneerlocal.com/oakbrook/index.html">The Doings Oak Brook Edition</a></li>
							<li><a href="http://www.pioneerlocal.com/burrridge/index.html">The Doings Weekly Edition</a></li>
							<li><a href="http://www.pioneerlocal.com/westernsprings/index.html">The Doings Western Springs Edition</a></li>
							<li><a href="http://www.pioneerlocal.com/deerfield/index.html">Deerfield Review</a></li>
							<li><a href="http://www.pioneerlocal.com/elmwoodpark/index.html">Elm Leaves</a></li>
							</ul>
						</div>
						
						<div class="divided-pioneer-pubs-two">
							<h4>&nbsp;</h4>
							<ul>
						  <li><a href="http://www.pioneerlocal.com/evanston/index.html">Evanston Review</a></li>
						  <li><a href="http://www.pioneerlocal.com/riverforest/index.html">Forest Leaves</a></li>
						  <li><a href="http://www.pioneerlocal.com/franklinpark/index.html">Franklin Park Herald - Journal</a></li>
						  <li><a href="http://www.pioneerlocal.com/glencoe/index.html">Glencoe News</a></li>
						  <li><a href="http://www.pioneerlocal.com/glenview/index.html">Glenview Announcements </a></li>              
						  <li><a href="http://www.pioneerlocal.com/highlandpark/index.html">Highland Park News</a></li>
						  <li><a href="http://www.pioneerlocal.com/lakeforest/index.html">Lake Forester</a></li>
						  <li><a href="http://www.pioneerlocal.com/lakezurich/index.html">Lake Zurich Courier</a></li>
						  <li><a href="http://www.pioneerlocal.com/libertyville/index.html">Libertyville Review</a></li>
						  <li><a href="http://www.pioneerlocal.com/lincolnshire/index.html">Lincolnshire Review</a></li>
						  <li><a href="http://www.pioneerlocal.com/lincolnwood/index.html">Lincolnwood Review</a></li>
						</ul>
						</div>
						<div class="divided-pioneer-pubs-two">
							<h4>&nbsp;</h4>
							<ul>
						  <li><a href="http://www.pioneerlocal.com/mortongrove/index.html">Morton Grove Champion</a></li>
						  <li><a href="http://www.pioneerlocal.com/mundelein/index.html">Mundelein Review</a></li>
						  <li><a href="http://www.pioneerlocal.com/niles/index.html">Niles Herald-Spectator</a></li>
						  <li><a href="http://www.pioneerlocal.com/norridge/index.html">Norridge-Harwood Heights News</a></li>
						  <li><a href="http://www.pioneerlocal.com/northbrook/index.html">Northbrook Star</a></li>
						  <li><a href="http://www.pioneerlocal.com/oakpark/index.html">Oak Leaves</a></li>
						  <li><a href="http://www.pioneerlocal.com/parkridge/index.html">Park Ridge Herald-Advocate</a></li>             
						  <li><a href="http://www.pioneerlocal.com/skokie/index.html">Skokie Review</a></li>
						  <li><a href="http://www.pioneerlocal.com/vernonhills/index.html">Vernon Hills Review</a></li>
						  <li><a href="http://www.pioneerlocal.com/wilmette/index.html">Wilmette Life</a></li>
						  <li><a href="http://www.pioneerlocal.com/winnetka/index.html">Winnetka Talk</a></li>
						  
						  </ul>
						</div>


						<div class="divided-wrapports-pubs">
							
							 <h4>AFFILIATES</h4>
							<ul>
						<li><a href="http://www.chicagoreader.com/">Chicago Reader</a></li>	
						<li><a href="http://seasonpass.suntimes.com/" title="SeasonPass.com">High School Sports</a></li>
						<li><a href="http://www.todrive.com/" title="Autos">Cars for Sale - ToDrive.com </a></li>
						<li><a href="http://searchchicago.suntimes.com/homes/index.html" title="Homes">SearchChicago - Homes</a></li>
						
						<li><a href="http://searchchicago.suntimes.com/directories/" title="Classifieds">SearchChicago - Directories</a></li> 
						
							
								</ul>
						</div>
			  </div>
			  
			  <div class="affiliateLogos">
				<div class="gridFooterLogo"><a href="http://www.chicagogrid.com/" target="_blank">Grid</a></div>
				<div class="splashFooterLogo"><a href="http://splash.suntimes.com/" target="_blank">Splash</a></div>
				
			  </div>


			</div>


				<div id="sub-footer-wrapper">
				  <div id="sub-footer-content">
				  <ul>
				  <li><a href="http://www.suntimesmediaholdings.com" class="stm-logo"><span class="hide">Sun-Times Media</span></a></li>
				<li><a class="stm-copyright" href="http://www.suntimesmediaholdings.com" title="Copyright 2013 Sun-Times Media, LLC">&#169; Copyright 2013 Sun-Times Media, LLC</a></li>
				</ul>
					  </div>
					</div>
				 </div>
			</div>

			

			<SCRIPT LANGUAGE=JavaScript><!--OAS_AD('Frame1');//--></SCRIPT>
			<SCRIPT LANGUAGE=JavaScript><!--OAS_AD('x02');//--></SCRIPT>





			<!-- **** footer END **** -->
			<!--/FileInclude:Normal, /csp/cms/sites/STM/assets/includes/footer.csp, took .00018 -->

				

				<a id="FBIPAD" href="http://www.suntimes.com/csp/cms/sites/STM/assets/includes/ipadSplash.csp" style="display:none;"></a>


		</div>

	</body>
</html>