<?php 
	get_header();
		$site = site_url() . "/wp-content/themes/reader/"; 
?>
	<!-- ROW 1 -->
		<div id="row1_container">
		<!--
		  <div class="row1_left">
			<section id="main">
				<img src="<?php echo $site; ?>images/slider1.jpg" class="slider" />
			</section>
		  </div>
		  
		  <div class="slider_box">
			<aside class="slider_box">
				<h1 class="description">Brindille is a refuse from frightening surroundings but conservative seasoning dulls and skilled execution</h1>
				<p class="description">Lead text. Lead text. Lets add some more lorem ipsum text. Hawks win in 3OT thriller</p>
				<p class="review">Review >></p>
			</aside>
		  </div>	
		  !-->
		  
		  <?php $result = file_get_contents('http://www.chicagoreader.com/chicago/Custom/SunTimes/Agenda/Lead.html'); ?>
				<?php echo($result); ?>
			
		</div>
	<!-- END ROW 1 -->

	<!-- ROW 2 -->
		<div id="row2_container">
			<section id="main2">
				<h1 class="reader_bg">&nbsp;</h1>
				 <div  class="agenda_box_left">
					<p> 
				<?php $result = file_get_contents('http://www.chicagoreader.com/chicago/Custom/SunTimes/Agenda.html'); ?>
				<?php echo($result); ?>
					</p>
				 </div>
				 <div class="agenda_box_right">
					<p class="buffer">
					
						<ul class="essentials">
							<li class="essentials"><img src="<?php echo $site; ?>/images/search_essentials_calendar.png"><p><a href="http://www.chicagoreader.com/chicago/Agenda"><h4 id="agenda_header">THE AGENDA CALENDAR</h4>Reader-recommended events this week</a></p></li>
							<li class="essentials"><img src="<?php echo $site; ?>/images/search_essentials_music.png"><p><a href="http://www.chicagoreader.com/chicago/Soundboard"><h4>SOUNDBOARD</h4>Recommended and notable concerts this week</a></p></li>
							<li class="essentials"><img src="<?php echo $site; ?>/images/search_essentials_music.png"><p><a href="http://www.chicagoreader.com/chicago/EarlyWarnings"><h4>EARLY WARNINGS</h4> Chicago concerts you should know about in the weeks to come</p></a></li>
							<li class="essentials"><img src="<?php echo $site; ?>/images/search_essentials_culture.png"><p><a href="http://www.chicagoreader.com/chicago/FindACulturalEvent/Page"><h4>FIND A CULTURAL EVENT</h4></a></p></li>
							<li class="essentials"><img src="<?php echo $site; ?>/images/search_essentials_food.png"><p><a href="http://www.chicagoreader.com/chicago/RestaurantFinder/Page"><h4>FIND A RESTAURANT</h4></a></p></li>
							<li class="essentials"><img src="<?php echo $site; ?>/images/search_essentials_wine.png"><p><a href="http://www.chicagoreader.com/chicago/BarGuide"><h4>FIND A BAR</h4></a></p></li>
							<li class="essentials"><img src="<?php echo $site; ?>/images/search_essentials_movie.png"><p><a href="http://www.chicagoreader.com/chicago/MovieTimes"><h4>FIND A MOVIE</h4></a></p></li>
							<li class="essentials"><img src="<?php echo $site; ?>/images/search_essentials_icecream.png"><p><a href="http://www.chicagoreader.com/chicago/summer-guide-2013-calendar-what-to-do/Content?oid=9607311"><h4>SUMMER GUIDE</h4></a></p></li>
						</ul>
					</p>
                    
                   
				</div>
               
               
			</section>
		
		<div class="cst_slider_box">
			<aside class="cst_slider_box">
            	
               
                
                
				<h1 class="sun_times_bg">&nbsp;</h1>
					<strong>TODAYS ENTERTAINMENT</strong>
					<p>
					<?php
					
						$rss = fetch_feed('http://www.suntimes.com/rss/index.html?vs=1&path=/suntimes/entertainment');
						$maxitems = $rss->get_item_quantity(3);
						$rss_items = $rss->get_items(0, $maxitems);
						?>
						<ul class="entertainment">
						<?php if ($maxitems == 0) echo '<li>No items.</li>';
						else
						// Loop through each feed item and display each item as a hyperlink.
						foreach ( $rss_items as $item ) : ?>
						 <?php $image = $item->get_description(); ?>
						<li>
						<a href='<?php echo $item->get_permalink(); ?>' title='<?php echo 'Posted '.$item->get_date('j F Y | g:i a'); ?>'>
						<?php echo str_replace('/csp', 'http://www.suntimes.com/csp', $item->get_description()); ?>
						</a>
						</li>
						<?php endforeach; ?>
						</ul>
					</p>
			</aside>
		</div>
		</div>
	<!-- END ROW 2 -->	
		
		<div style="clear:both;"></div>
	
    <!-- ROW 3 -->	
	
			<section id="main3">
            	<div class="agenda_bg"></div>
                <div class="video-container">
		<?php $cat_id = 2; //the certain category ID
                 $latest_cat_post = new WP_Query( array('posts_per_page' => 1, 'category__in' => array($cat_id)));
                 if( $latest_cat_post->have_posts() ) : while( $latest_cat_post->have_posts() ) : $latest_cat_post->the_post();  ?>

                  <?php the_content(); ?>

                  <?php endwhile; endif; ?>
                 </div>
                 <div style="clear:both;"></div>
			</section>
		
			<aside>
			 <div id="rightside_ad">
				<script type="text/javascript"> OAS_AD("TopRight");</script>
			 </div>
			</aside>
			<div style="clear:both;"></div>
			
			<?php get_footer(); ?>