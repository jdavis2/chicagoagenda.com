<?php 
	$site = site_url() . "/wp-content/themes/reader/"; 
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8"/>
		<title> Reader Agenda | Welcome</title>
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
		<link rel="shortcut icon" href="http://www.chicagoreader.com/favicon.ico" />
		<link href="<?php echo $site; ?>style.css" type="text/css" rel="stylesheet">
		<!--[if lt IE 9]>
		<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<script type='text/javascript' src='scripts/respond.min.js'></script>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script type="text/javascript" src="http://www.suntimes.com/csp/cms/sites/STM/assets/js/pluginsFULL.js"></script>
	<script type="text/javascript" src="http://www.suntimes.com/csp/cms/sites/STM/assets/js/external_feeds.js?20130619a"></script>  
	<script type="text/javascript" src="http://www.suntimes.com/csp/cms/sites/STM/assets/js/page-load.js?20130619c"></script>	 
	<script type="text/javascript" src="http://www.suntimes.com/csp/cms/sites/STM/assets/js/superfish.js"></script> 
	
	 <script type="text/javascript">
			$(document).ready(function() {
			  $.get("http://www.chicagoreader.com/chicago/Custom/Toc/SunTimesAgenda.html", function(data) {
				var child_body = $(data).find("<ul class=\"agenda\">");
				$("#sun-times-agenda").append(child_body);
			  });
			});
	</script>

	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
		{ (i[r].q=i[r].q||[]).push(arguments)}
		,i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-54260868-1', 'auto',
		{name: 'agenda'}
		);
		ga('agenda.require', 'displayfeatures');
		ga('agenda.send', 'pageview');
		ga('create', 'UA-53290409-1', 'auto',
		{name: 'networkGlobal'}
		); // New tracker.
		ga('networkGlobal.require', 'displayfeatures');
		ga('networkGlobal.send', 'pageview');
	</script>

	<!-- OAS SETUP start 
	<script type="text/javascript" src="http://www.suntimes.com/csp/cms/sites/STM/assets/js/oas-splash.js"></script>
		 OAS SETUP end -->
	
    <!-- Start Omniture SiteCatalyst -->
	<script type="text/javascript" src="http://www.suntimes.com/csp/cms/sites/STM/assets/js/s-code.js"></script>
    <!-- End SiteCatalyst code -->

    <?php include('google-ad-boilerplate.php'); ?>
    <?php include('google-ad-placements.php'); ?>
	</head>
	
	<body>
		
		<div id="wrapper">
		
			<header>
				
				<nav id="skipTo">
					<ul>
						<li>
							<a href="#main">Mobile View</a>
						</li>
					</ul>
					
					
				</nav>
				
				<div id="banner">
					 <div class="leaderboard">
						<div id="top1" style="text-align:center;margin-top:10px;">
							<script type="text/javascript">//OAS_AD("Top1");</script>
							<div id='div-gpt-ad-top1'>
								<script type='text/javascript'>
									googletag.cmd.push(function() {
										googletag.display('div-gpt-ad-top1');
									});
								</script>
							</div>

						</div>
					<img src="<?php echo $site; ?>images/header_logo.png" class="banner" alt="banner" /> 
					</div>
				</div>
				
			</header>	
		
		<div style="clear:both;"></div>